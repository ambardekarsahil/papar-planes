//
//  OpenCVWrapper.m
//  PapAR Planes
//
//  Created by Sahil Ambardekar on 9/23/17.
//  Copyright © 2017 mhacksx. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"

@implementation OpenCVWrapper

/*
- (cv::Mat) convertBuffertoMat: (CVImageBufferRef)imageBuffer {
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);

    cv::Mat frame((int) height, (int) width, CV_8UC4, (void*)baseAddress);
    return frame;
}
*/

// (-1,-1,-1,-1) if not found
- (UIImage *) findPlane: (CVImageBufferRef)imageBuffer {
    
    
    /*Lock the image buffer*/
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    
    
    /*Get information about the image*/
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    //TODO: dynamic width and height
    cv::Mat mat((int) height, (int) width, CV_8UC1, (void*)baseAddress);
    
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    cv::Mat n;
    cv::threshold(mat, mat, 185, 255, CV_THRESH_BINARY_INV);
    
    cv::SimpleBlobDetector::Params params;

    params.filterByInertia = true;
    params.minInertiaRatio = 0.05;
    params.maxInertiaRatio = 0.3;
    params.filterByCircularity = true;
    /*
    params.minCircularity = 0;
    params.maxCircularity = 0.4;
    params.filterByConvexity = true;
    params.minConvexity = 0;
    params.maxConvexity = 0.8;
    */
    params.filterByArea = true;
    params.minArea = 50;
    params.maxArea = 3000;
    
    
    
    
    cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
    std::vector<cv::KeyPoint> keypoints;
    detector->detect( mat, keypoints);
    std::cout << keypoints.size() << " kp size" << std::endl;

    for (int i = 0; i < keypoints.size(); i++) {
        std::cout << "point " << i << " " << keypoints[i].pt << std::endl;
        std::cout << "size " << i << " " << keypoints[i].size << std::endl;
    }
    
    
    NSData *data = [NSData dataWithBytes:mat.data length:mat.elemSize()*mat.total()];
    CGColorSpaceRef colorSpace;
    
    if (mat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(mat.cols,                                 //width
                                        mat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * mat.elemSize(),                       //bits per pixel
                                        mat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNone|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);

    
    
    return finalImage;
}

@end
