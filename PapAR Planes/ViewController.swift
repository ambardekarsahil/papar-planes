//
//  ViewController.swift
//  PapAR Planes
//
//  Created by Sahil Ambardekar on 9/23/17.
//  Copyright © 2017 mhacksx. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var forces: [CGFloat] = []
    
    var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene
        imageView = UIImageView(frame: view.frame)
        view.addSubview(imageView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
        sceneView.automaticallyUpdatesLighting = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }

    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        
//        if let image: CVImageBuffer = sceneView.session.currentFrame?.capturedImage {
//            let openCVWrapper = OpenCVWrapper()
//            let planeRect = openCVWrapper.findPlane(image)
//            let main = DispatchQueue.main
//            main.async {
//                self.imageView.image = planeRect
//            }
//
//            /*if planeRect.size.width != -1 {
//                // supposedly found the plane at this rect
//            } else {
//                // rip, supposedly no plane in rect
//            }*/
//        }
 
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        let scene = SCNScene(named: "art.scnassets/ring.dae")
        let n = scene?.rootNode.childNode(withName: "ring", recursively: true)
        node.addChildNode(n!)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
           forces.append(touch.force)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            forces.append(touch.force)
            if let currentFrame = sceneView.session.currentFrame {
                // Add new anchor to AR session
                var translation = matrix_identity_float4x4
                var index = forces.count - 4
                if index < 0 {
                    index = forces.count - 1
                }
                translation.columns.3.z = -1 + -4 * Float(forces[index] / touches.first!.maximumPossibleForce)
                let transform = simd_mul(currentFrame.camera.transform, translation)
                let anchor = ARAnchor(transform: transform)
                sceneView.session.add(anchor: anchor)
            }
        }
        forces = []
    }
}
