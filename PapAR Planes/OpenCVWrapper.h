//
//  OpenCVWrapper.h
//  PapAR Planes
//
//  Created by Sahil Ambardekar on 9/23/17.
//  Copyright © 2017 mhacksx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ARKit/ARKit.h>
#import <CoreGraphics/CoreGraphics.h>

@interface OpenCVWrapper : NSObject

- (UIImage *) findPlane: (CVImageBufferRef)imageBuffer;

@end
